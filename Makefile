../daemons.tar: $(wildcard *)
	tar -cf ../daemons.tar . --xform='s:^:daemons/:'
../daemons.tar.gz: ../daemons.tar
	rm -f $@
	gzip ../daemons.tar

upload: ../daemons.tar.gz
	scp $< kaction@people.debian.org:public_html

