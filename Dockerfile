FROM debian:9
RUN echo "deb http://cdn-fastly.deb.debian.org/debian unstable main" | tee /etc/apt/sources.list
# Docker best practices recommend removing apt lists, but our use case is different.
RUN apt-get update && apt-get dist-upgrade -y
# At least init.d script of Tor is buggy.
RUN apt-get install -y procps
